import React, { useRef, useState } from "react";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/swiper.min.css";
import "swiper/components/pagination/pagination.min.css";
import "swiper/components/navigation/navigation.min.css";

// import Swiper core and required modules
import SwiperCore, { Pagination, Navigation } from "swiper/core";

// install Swiper modules
SwiperCore.use([Pagination, Navigation]);

export default class Slider extends React.Component {
  render() {
    return (
      <>
        <Swiper
          className="p-2 whitespace-nowrap mt-12 mySwiper "
          watchSlidesProgress={true}
          watchSlidesVisibility={true}
          spaceBetween={30}
          dir="rtl"
          speed={500}
          breakpoints={{
            // when window width is >= 640px
            320: {
              slidesPerView: 2,
              spaceBetween: 20,
            },
            // when window width is >= 768px
            768: {
              slidesPerView: 3,
              spaceBetween: 30,
            },
            1024: {
              slidesPerView: 3,
              spaceBetween: 50,
            },
            1209: {
              slidesPerView: 4,
            },
            1480: {
              slidesPerView: 4,
            },
          }}
        >
          <SwiperSlide>
            <div className="w-full  lg:w-60 border ml-2 rounded-md text-right ">
              {" "}
              <a href="#">
                <img
                  className="w-full  rounded-t-md rounded-lg"
                  src="/images/agahi1.jpg"
                />
                <div className="p-4 w-full">
                  <span
                    className="text-sm "
                    style={{ color: "#000000", fontWeight: "900" }}
                  >
                    نیسان پاترول ۴در
                  </span>
                </div>
                <div className="px-4 pb-4 w-full" style={{ color: "#707070" }}>
                  <span className="font-extralight text-xs ">ساخت ۱۳۶۸</span>
                  <br />
                  <span className="font-extralight  text-xs">
                    ۱۴۹،۰۰۰،۰۰۰ تومان
                  </span>
                </div>
              </a>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="w-full lg:w-60 border ml-2 rounded-md text-right ">
              {" "}
              <a href="#">
                <img className="w-full rounded-t-md" src="/images/agahi2.jpg" />
                <div className="p-4">
                  <span className="text-sm" style={{ color: "#000000" }}>
                    نیسان پاترول ۴در
                  </span>
                </div>
                <div className="px-4 pb-4 w-full" style={{ color: "#707070" }}>
                  <span className="font-extralight  text-xs">ساخت ۱۳۶۸</span>
                  <br />
                  <span className="font-extralight  text-xs">
                    ۱۴۹،۰۰۰،۰۰۰ تومان
                  </span>
                </div>
              </a>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="w-full lg:w-60 border ml-2 rounded-md text-right">
              {" "}
              <a href="#">
                <img className="w-full rounded-t-md" src="/images/agahi3.jpg" />
                <div className="p-4">
                  <span className="text-sm" style={{ color: "#000000" }}>
                    نیسان پاترول ۴در
                  </span>
                </div>
                <div className="px-4 pb-4 w-full" style={{ color: "#707070" }}>
                  <span className="font-extralight  text-xs">ساخت ۱۳۶۸</span>
                  <br />
                  <span className="font-extralight  text-xs">
                    ۱۴۹،۰۰۰،۰۰۰ تومان
                  </span>
                </div>
              </a>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="w-full lg:w-60 border ml-2 rounded-md text-right">
              {" "}
              <a href="#">
                <img className="w-full rounded-t-md" src="/images/agahi1.jpg" />
                <div className="p-4">
                  <span className="text-sm" style={{ color: "#000000" }}>
                    نیسان پاترول ۴در
                  </span>
                </div>
                <div className="px-4 pb-4 w-full" style={{ color: "#707070" }}>
                  <span className="font-extralight  text-xs">ساخت ۱۳۶۸</span>
                  <br />
                  <span className="font-extralight  text-xs">
                    ۱۴۹،۰۰۰،۰۰۰ تومان
                  </span>
                </div>
              </a>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="w-full lg:w-60 border ml-2 rounded-md text-right">
              {" "}
              <a href="#">
                <img className="w-full rounded-t-md" src="/images/agahi2.jpg" />
                <div className="p-4">
                  <span className="text-sm" style={{ color: "#000000" }}>
                    نیسان پاترول ۴در
                  </span>
                </div>
                <div className="px-4 pb-4 w-full" style={{ color: "#707070" }}>
                  <span className="font-extralight  text-xs">ساخت ۱۳۶۸</span>
                  <br />
                  <span className="font-extralight  text-xs">
                    ۱۴۹،۰۰۰،۰۰۰ تومان
                  </span>
                </div>
              </a>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="w-full lg:w-60 border ml-2 rounded-md text-right">
              {" "}
              <a href="#">
                <img className="w-full rounded-t-md" src="/images/agahi3.jpg" />
                <div className="p-4">
                  <span className="text-sm" style={{ color: "#000000" }}>
                    نیسان پاترول ۴در
                  </span>
                </div>
                <div className="px-4 pb-4 w-full" style={{ color: "#707070" }}>
                  <span className="font-extralight  text-xs">ساخت ۱۳۶۸</span>
                  <br />
                  <span className="font-extralight  text-xs">
                    ۱۴۹،۰۰۰،۰۰۰ تومان
                  </span>
                </div>
              </a>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="w-full lg:w-60 border ml-2 rounded-md text-right ">
              {" "}
              <a href="#">
                <img
                  className="w-full  rounded-t-md"
                  src="/images/agahi1.jpg"
                />
                <div className="p-4 w-full">
                  <span className="text-sm" style={{ color: "#000000" }}>
                    نیسان پاترول ۴در
                  </span>
                </div>
                <div className="px-4 pb-4 w-full" style={{ color: "#707070" }}>
                  <span className="font-extralight  text-xs">ساخت ۱۳۶۸</span>
                  <br />
                  <span className="font-extralight  text-xs">
                    ۱۴۹،۰۰۰،۰۰۰ تومان
                  </span>
                </div>
              </a>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="w-full lg:w-60 border ml-2 rounded-md text-right">
              {" "}
              <a href="#">
                <img className="w-full rounded-t-md" src="/images/agahi2.jpg" />
                <div className="p-4">
                  <span className="text-sm" style={{ color: "#000000" }}>
                    نیسان پاترول ۴در
                  </span>
                </div>
                <div className="px-4 pb-4 w-full" style={{ color: "#707070" }}>
                  <span className="font-extralight  text-xs">ساخت ۱۳۶۸</span>
                  <br />
                  <span className="font-extralight  text-xs">
                    ۱۴۹،۰۰۰،۰۰۰ تومان
                  </span>
                </div>
              </a>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="w-full lg:w-60 border ml-2 rounded-md text-right">
              {" "}
              <a href="#">
                <img className="w-full rounded-t-md" src="/images/agahi3.jpg" />
                <div className="p-4">
                  <span className="text-sm" style={{ color: "#000000" }}>
                    نیسان پاترول ۴در
                  </span>
                </div>
                <div className="px-4 pb-4 w-full" style={{ color: "#707070" }}>
                  <span className="font-extralight  text-xs">ساخت ۱۳۶۸</span>
                  <br />
                  <span className="font-extralight  text-xs">
                    ۱۴۹،۰۰۰،۰۰۰ تومان
                  </span>
                </div>
              </a>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="w-full lg:w-60 border ml-2 rounded-md text-right">
              {" "}
              <a href="#">
                <img className="w-full rounded-t-md" src="/images/agahi1.jpg" />
                <div className="p-4">
                  <span className="text-sm" style={{ color: "#000000" }}>
                    نیسان پاترول ۴در
                  </span>
                </div>
                <div className="px-4 pb-4 w-full" style={{ color: "#707070" }}>
                  <span className="font-extralight  text-xs">ساخت ۱۳۶۸</span>
                  <br />
                  <span className="font-extralight  text-xs">
                    ۱۴۹،۰۰۰،۰۰۰ تومان
                  </span>
                </div>
              </a>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="w-full lg:w-60 border ml-2 rounded-md text-right">
              {" "}
              <a href="#">
                <img className="w-full rounded-t-md" src="/images/agahi2.jpg" />
                <div className="p-4">
                  <span className="text-sm" style={{ color: "#000000" }}>
                    نیسان پاترول ۴در
                  </span>
                </div>
                <div className="px-4 pb-4 w-full" style={{ color: "#707070" }}>
                  <span className="font-extralight  text-xs">ساخت ۱۳۶۸</span>
                  <br />
                  <span className="font-extralight  text-xs">
                    ۱۴۹،۰۰۰،۰۰۰ تومان
                  </span>
                </div>
              </a>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="w-full lg:w-60 border ml-2 rounded-md text-right">
              {" "}
              <a href="#">
                <img className="w-full rounded-t-md" src="/images/agahi3.jpg" />
                <div className="p-4">
                  <span className="text-sm" style={{ color: "#000000" }}>
                    نیسان پاترول ۴در
                  </span>
                </div>
                <div className="px-4 pb-4 w-full" style={{ color: "#707070" }}>
                  <span className="font-extralight  text-xs  ">ساخت ۱۳۶۸</span>
                  <br />
                  <span className="font-extralight  text-xs  ">
                    ۱۴۹،۰۰۰،۰۰۰ تومان
                  </span>
                </div>
              </a>
            </div>
          </SwiperSlide>
        </Swiper>
      </>
    );
  }
}
