import Head from "next/head";
import Link from "next/link";
import Carousel from "./slider";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import Slider from "./agahiSlider";

import DesktopNav from "../Component/DesktopNav";
import MobileNav from "../Component/MobileNav";
import SubjectLink from "../Component/SubjectLink";
import TextArea from "../Component/TextArea";
import Karnameh from "../Component/Karnameh";
import SafeBuy from "../Component/SafeBuy";
import Name from "../Component/Name";
import TagLink from "../Component/TagLink";
import ContactButton from "../Component/ContactButton";
import GeneralDetails from "../Component/GeneralDetails";
import AllDetails from "../Component/AllDetails";
import Description from "../Component/Description";
import Tag from "../Component/Tag";
import MoreAds from "../Component/MoreAds";
import Footer from "../Component/Footer";
import MobileFooter from "../Component/MobileFooter";

export default function Home() {
  return (
    <div className="mb-7">
      <Head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <title>DIVAR</title>
      </Head>

      <DesktopNav />
      <MobileNav />

      <section className=" z-30 pt-20 flex-col justify-around px-2">
        <SubjectLink />
        <div className="flex md:flex-row justify-around flex-col lg:ml-0">
          <div className=" text-right w-full md:w-1/2 xl:w-2/5  ">
            <div className="w-full  pb-2">
              <Carousel className="rounded" />
            </div>
            <div className="md:visible  invisible absolute md:static">
              <TextArea />
            </div>
            <div className="border rounded mb-8 mt-4 md:visible  invisible absolute md:static px-2">
              <Karnameh />
            </div>
            <div
              className=" md:visible  invisible absolute md:static
 md:visible  invisible absolute md:static"
            >
              <SafeBuy />
            </div>
          </div>
          <div className=" text-right w-full md:w-1/3 xl:w-2/5  ">
            <div className="-mt-1 leading-10">
              <Name />
              <TagLink />
            </div>
            <div>
              <ContactButton />
            </div>
            <GeneralDetails />
            <AllDetails />
            <Description />
            <Tag />
            <div className="visible  md:invisible md:absolute static text-right">
              <TextArea />
            </div>
          </div>
          <div
            style={{ color: "#707070" }}
            className="md:invisible md:absolute static text-right text-sm "
          >
            <SafeBuy />
          </div>
        </div>
      </section>
      <section className="  object-cover lg:w-200 mt-12 overflow-hidden  md:px-16 px-4 ">
        <MoreAds />
      </section>
      <Footer />
      <MobileFooter />
    </div>
  );
}
