import React, { useState } from "react";
import Slider from "./agahiSlider";
import { SocialIcon } from "react-social-icons";

export default () => {
  const [data, setData] = useState(["1"]);

  const [showAll, setShowAll] = useState(false);
  const [showCurrent, setShowCurrent] = useState(false);

  const toggleAll = () => {
    setShowAll((val) => !val);
    setShowCurrent(false);
  };

  return (
    <div className=" -mt-20  md:invisible  visible static md:absolute">
      <div>
        <button onClick={toggleAll}>
          {showAll ? (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-6 w-6"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M5 15l7-7 7 7"
              />
            </svg>
          ) : (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-6 w-6"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M19 9l-7 7-7-7"
              />
            </svg>
          )}
        </button>
      </div>
      <div className="mt-20 mb-16">
        {showAll &&
          data.map(() => (
            <div>
              <div className="text-base">
                <div className="flex justify-between border-t-2 mt-6 p-3 ">
                  <a href="#" style={{ color: "#a62626" }}>
                    نیسان، پاترول 4 در، 4 سیلندر
                  </a>
                  <span style={{ color: "#707070" }}>برند و مدل</span>
                </div>
                <div className="flex justify-between border-t-2  p-3">
                  <span style={{ color: "#212121" }}>سالم</span>
                  <span style={{ color: "#707070" }}>وضعیت موتور</span>
                </div>
                <div className="flex justify-between border-t-2  p-3">
                  <span style={{ color: "#212121" }}>۳ ماه</span>
                  <span style={{ color: "#707070" }}>مهلت بیمهٔ شخص ثالث</span>
                </div>
                <div className="flex justify-between border-t-2  p-3">
                  <span style={{ color: "#212121" }}>دنده‌ای</span>
                  <span style={{ color: "#707070" }}>گیربکس</span>
                </div>
                <div className="flex justify-between border-t-2  p-3">
                  <span style={{ color: "#212121" }}>تک برگی</span>
                  <span style={{ color: "#707070" }}>سند</span>
                </div>
                <div className="flex justify-between border-t-2  p-3">
                  <span style={{ color: "#212121" }}>نقدی</span>
                  <span style={{ color: "#707070" }}>نحوه فروش</span>
                </div>
                <div className="flex justify-between border-t-2 border-b-2  p-3 ">
                  <div>
                    <span style={{ color: "#212121" }}>۱۴۵٫۰۰۰٫۰۰۰</span>
                    <span style={{ color: "#212121" }}>تومان</span>
                  </div>

                  <span style={{ color: "#707070" }}>قیمت</span>
                </div>
              </div>
              <div
                className="mt-8 space-y-8 leading-relaxed "
                style={{ color: "#212121" }}
              >
                <span className="text-xl font-black "> توضیحات</span>
                <br />
                <br />
                <span className="font-thin text-base ">
                  پاترول چهار در چهار سیلندر
                </span>
                <br />
                <span className="font-thin text-base ">فنی عالی</span>
                <br />
                <span className="font-thin text-base ">
                  تودوزی پاجیرویی و کنسول وسط
                </span>
                <br />
                <span className="font-thin text-base  ">
                  لاستیک جلو صد درصد
                </span>
                <br />
                <span className="font-thin text-base ">
                  اطاق تعویض قید در سند
                </span>
                <br />
              </div>
              <div className="mt-8 mb-8">
                <button
                  className="btnActiveRed  px-3 py-1 text-sm rounded-xl"
                  style={{
                    color: "white",
                    backgroundColor: "#a62626",
                    opacity: "85%",
                    outlineStyle: "none",
                  }}
                >
                  سواری در استخر
                </button>
                <button
                  className="btnActiveRed ml-2 px-3 py-1 text-sm text-white rounded-xl"
                  style={{
                    backgroundColor: "#a62626",
                    opacity: "85%",
                    outlineStyle: "none",
                  }}
                >
                  سواری
                </button>
              </div>
              <div
                style={{ color: "#707070" }}
                className="flex justify-end  font-light border-b-2 pb-4 mt-4 visible  md:invisible md:absolute static text-right text-sm "
              >
                {" "}
                راهنمای خرید امن
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-6 w-6 ml-1 btnActiveRed"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                  style={{ color: "#a62626", opacity: "80%" }}
                >
                  <path
                    fillRule="evenodd"
                    d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-8-3a1 1 0 00-.867.5 1 1 0 11-1.731-1A3 3 0 0113 8a3.001 3.001 0 01-2 2.83V11a1 1 0 11-2 0v-1a1 1 0 011-1 1 1 0 100-2zm0 8a1 1 0 100-2 1 1 0 000 2z"
                    clipRule="evenodd"
                  />
                </svg>
              </div>
              <div
                className="flex justify-end font-light border-b-2 py-4 visible  md:invisible md:absolute static text-sm"
                style={{ color: "#707070" }}
              >
                گزارش مشکل آگهی
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-6 w-6 ml-1 btnActiveRed"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                  style={{ color: "#a62626", opacity: "80%" }}
                >
                  <path
                    fillRule="evenodd"
                    d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
                    clipRule="evenodd"
                  />
                </svg>
              </div>
              <div className=" text-right  ">
                <span
                  className="font-semibold text-lg"
                  style={{ color: "#212121" }}
                >
                  آگهی‌های مشابه
                </span>
                <Slider className="flex flex-nowrap rounded " />
              </div>
            </div>
          ))}
      </div>
    </div>
  );
};
