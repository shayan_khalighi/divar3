import React from "react";
import { Carousel } from "react-responsive-carousel";

export default () => (
  <Carousel showThumbs={false} className="rounded-lg ">
    <div>
      <img alt="" src="/images/1.jpg" className="rounded-lg " />
    </div>
    <div>
      <img alt="" src="/images/2.jpg " />
    </div>
    <div>
      <img alt="" src="/images/3.jpg" className="rounded-lg " />
    </div>

    <div>
      <img alt="" src="/images/4.jpg" className="rounded-lg " />
    </div>
    <div>
      <img alt="" src="/images/5.jpg" className="rounded-lg " />
    </div>
    <div>
      <img alt="" src="/images/6.jpg" className="rounded-lg " />
    </div>
  </Carousel>
);
