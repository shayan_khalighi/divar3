import React from "react";

function SubjectLink() {
  return (
    <div
      className=" pr-4 text-right  text-xs leading-relaxed"
      style={{ color: "#707070" }}
    >
      وسایل نقلیه{" > "}
      خودرو{" > "}
      سواری{" > "}
      نیسان{" > "}
      پاترول 4 در{" > "}4 سیلندر{" > "}
      نیسان پاترول 4 در 4 سیلندر، مدل ۱۳۷۸
    </div>
  );
}

export default SubjectLink;
