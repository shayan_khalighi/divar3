import React from "react";

function Karnameh() {
  return (
    <div>
      <div className="flex justify-between border-b-2">
        <img
          className="h-20 ml-4"
          src="/images/karnameh-logo.png"
          alt="karnameh"
        />
        <p className="p-4">
          <span className="text-xl " style={{ color: "#212121" }}>
            کارشناسی با کارنامه
          </span>{" "}
          <br />
          <span className="text-sm font-light " style={{ color: "#707070" }}>
            با هزینهٔ ۲۵۰٬۰۰۰ تومان
          </span>
        </p>
      </div>
      <div
        className="text-sm font-light mx-4 mt-4"
        style={{ color: "#707070" }}
      >
        کارشناسان کارنامه با تجهیزات کامل و از طرف شما این خودرو را کارشناسی
        می‌کنند و گزارش آن را برای شما می‌فرستند
      </div>
      <button
        className="  w-full text-base  border  rounded h-10 w-64 mr-2 my-4"
        style={{
          borderColor: "#a62626",
          color: "#a62626",
          outlineStyle: "none",
        }}
      >
        اطلاعات بیشتر و درخواست کارشناسی
      </button>
    </div>
  );
}

export default Karnameh;
