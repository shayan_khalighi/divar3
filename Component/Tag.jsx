import React from "react";

function Tag() {
  return (
    <div>
      <div className="mt-8 mb-8">
        <button
          className="btnActiveRed  px-3 py-1 text-sm rounded-xl"
          style={{
            color: "white",
            backgroundColor: "#a62626",
            opacity: "85%",
            outlineStyle: "none",
          }}
        >
          سواری در استخر
        </button>
        <button
          className="btnActiveRed ml-2 px-3 py-1 text-sm text-white rounded-xl"
          style={{
            backgroundColor: "#a62626",
            opacity: "85%",
            outlineStyle: "none",
          }}
        >
          سواری
        </button>
      </div>
    </div>
  );
}

export default Tag;
