import React from "react";

function GeneralDetails() {
  return (
    <div>
      <div className=" flex flex-row justify-around text-center mt-8">
        <div className=" w-1/3  ">
          <span className="font-hairline text-sm" style={{ color: "#707070" }}>
            رنگ
          </span>{" "}
          <br />
          <span className="font-medium text-lg " style={{ color: "#212121" }}>
            سبز
          </span>
        </div>
        <div className=" border-l-2 border-r-2  w-1/3 ">
          <span className="font-hairline  text-sm" style={{ color: "#707070" }}>
            سال ساخت
          </span>{" "}
          <br />
          <span className="font-medium text-lg " style={{ color: "#212121" }}>
            ۱۳۷۸
          </span>
        </div>
        <div className="w-1/3 ">
          <span className="font-hairline text-sm" style={{ color: "#707070" }}>
            کارکرد
          </span>{" "}
          <br />
          <span className="font-medium text-lg " style={{ color: "#212121" }}>
            ۳۰۰,۰۰۰
          </span>
        </div>
      </div>
    </div>
  );
}

export default GeneralDetails;
