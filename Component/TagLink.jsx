import React from "react";

function TagLink() {
  return (
    <span className=" text-base" style={{ color: "#707070" }}>
      لحظاتی پیش در تهران، استخر | سواری
    </span>
  );
}

export default TagLink;
