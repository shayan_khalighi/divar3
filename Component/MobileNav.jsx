export default function MobileNav() {
  return (
    <nav className="bg-opacity-95 bg-red-800 shadow-md w-full  fixed inset-0 z-50  h-16  bg-white md:invisible visible flex justify-center items-center">
      <h3 className=" text-white flex  justify-self-center  ">
        {" "}
        نیسان پاترول 4 در 4 سیلندر، مدل ۱۳۷۸
      </h3>

      <svg
        className="h-7 w-7  absolute right-3"
        width="24"
        height="24"
        viewBox="0 0 24 24"
        strokeWidth="2"
        stroke="currentColor"
        fill="none"
        strokeLinecap="round"
        strokeLinejoin="round"
        style={{ color: "white" }}
      >
        {" "}
        <path stroke="none" d="M0 0h24v24H0z" />{" "}
        <line x1="5" y1="12" x2="19" y2="12" />{" "}
        <line x1="15" y1="16" x2="19" y2="12" />{" "}
        <line x1="15" y1="8" x2="19" y2="12" />
      </svg>
    </nav>
  );
}
