import React from "react";
import Slider from "../pages/agahiSlider";

function MoreAds() {
  return (
    <div>
      <div className=" text-right  ">
        <span className="font-semibold text-lg" style={{ color: "#212121" }}>
          آگهی‌های مشابه
        </span>
        <Slider className="flex flex-nowrap rounded " />
      </div>
    </div>
  );
}

export default MoreAds;
