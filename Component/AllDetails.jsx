import React from "react";

function AllDetails() {
  return (
    <div>
      <div className="text-base">
        <div className="flex justify-between border-t-2 mt-6 p-3 ">
          <a href="#" style={{ color: "#a62626" }}>
            نیسان، پاترول 4 در، 4 سیلندر
          </a>
          <span style={{ color: "#707070" }}>برند و مدل</span>
        </div>
        <div className="flex justify-between border-t-2  p-3">
          <span style={{ color: "#212121" }}>سالم</span>
          <span style={{ color: "#707070" }}>وضعیت موتور</span>
        </div>
        <div className="flex justify-between border-t-2  p-3">
          <span style={{ color: "#212121" }}>۳ ماه</span>
          <span style={{ color: "#707070" }}>مهلت بیمهٔ شخص ثالث</span>
        </div>
        <div className="flex justify-between border-t-2  p-3">
          <span style={{ color: "#212121" }}>دنده‌ای</span>
          <span style={{ color: "#707070" }}>گیربکس</span>
        </div>
        <div className="flex justify-between border-t-2  p-3">
          <span style={{ color: "#212121" }}>تک برگی</span>
          <span style={{ color: "#707070" }}>سند</span>
        </div>
        <div className="flex justify-between border-t-2  p-3">
          <span style={{ color: "#212121" }}>نقدی</span>
          <span style={{ color: "#707070" }}>نحوه فروش</span>
        </div>
        <div className="flex justify-between border-t-2 border-b-2  p-3 ">
          <div>
            <span style={{ color: "#212121" }}>۱۴۵٫۰۰۰٫۰۰۰</span>
            <span style={{ color: "#212121" }}>تومان</span>
          </div>

          <span style={{ color: "#707070" }}>قیمت</span>
        </div>
      </div>
    </div>
  );
}

export default AllDetails;
