import React from "react";

function MobileFooter() {
  return (
    <div>
      <footer
        className="z-50 bg-white bg-opacity-95 border-t-2 pt-2  h-10  text-center md:invisible  visible static md:absolute  fixed inset-x-0 bottom-0"
        style={{ backgroundColor: "#a62626" }}
      >
        <div className="w-full flex justify-evenly">
          <div className="relative">
            <div className="p-1 bg-white bg-opacity-100  rounded-full absolute  -bottom-3 -left-12">
              <div className="w-full h-full  rounded-full border-double border-1 ">
                <button
                  className="btnActiveRed text-white text-base  rounded-full w-12 h-12  flex justify-center items-center focus:outline-none"
                  style={{ backgroundColor: "#a62626", outlineStyle: "none" }}
                >
                  <svg
                    className="h-7 w-7 btnActiveRed focus:outline-none"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    strokeWidth="2"
                    stroke="currentColor"
                    fill="none"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  >
                    {" "}
                    <path stroke="none" d="M0 0h24v24H0z" />{" "}
                    <circle cx="6" cy="12" r="3" />{" "}
                    <circle cx="18" cy="6" r="3" />{" "}
                    <circle cx="18" cy="18" r="3" />{" "}
                    <line x1="8.7" y1="10.7" x2="15.3" y2="7.3" />{" "}
                    <line x1="8.7" y1="13.3" x2="15.3" y2="16.7" />
                  </svg>
                </button>
              </div>
            </div>
          </div>
          <div className="relative">
            <div className="p-1 bg-white bg-opacity-100  rounded-full absolute -bottom-3 -left-9">
              <div className="w-full h-full  rounded-full border-double border-1 ">
                <button
                  className="btnActiveRed text-white text-base  rounded-full w-12 h-12 flex justify-center items-center focus:outline-none"
                  style={{ backgroundColor: "#a62626", outlineStyle: "none" }}
                >
                  <svg
                    className="h-7 w-7 btnActiveRed focus:outline-none"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    strokeWidth="2"
                    stroke="currentColor"
                    fill="none"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  >
                    {" "}
                    <path stroke="none" d="M0 0h24v24H0z" />{" "}
                    <path d="M16 6h3a1 1 0 0 1 1 1v11a2 2 0 0 1 -4 0v-13a1 1 0 0 0 -1 -1h-10a1 1 0 0 0 -1 1v12a3 3 0 0 0 3 3h11" />{" "}
                    <line x1="8" y1="8" x2="12" y2="8" />{" "}
                    <line x1="8" y1="12" x2="12" y2="12" />{" "}
                    <line x1="8" y1="16" x2="12" y2="16" />
                  </svg>
                </button>
              </div>
            </div>
          </div>
          <div className="relative">
            <div className="p-1 bg-white bg-opacity-100  rounded-full absolute -bottom-3 -left-5">
              <div className="w-full h-full  rounded-full border-double border-1 ">
                <button
                  className="btnActiveRed text-white text-base rounded-full w-12 h-12 flex justify-center items-center focus:outline-none"
                  style={{ backgroundColor: "#a62626", outlineStyle: "none" }}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-8 w-8 btnActiveRed focus:outline-none"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M8 12h.01M12 12h.01M16 12h.01M21 12c0 4.418-4.03 8-9 8a9.863 9.863 0 01-4.255-.949L3 20l1.395-3.72C3.512 15.042 3 13.574 3 12c0-4.418 4.03-8 9-8s9 3.582 9 8z"
                    />
                  </svg>
                </button>
              </div>
            </div>
          </div>
          <div className="relative">
            <div className="p-1 bg-white bg-opacity-100  rounded-full absolute -left-2 -bottom-3">
              <div className="w-full h-full  rounded-full border-double border-1 ">
                <button
                  className="btnActiveRed text-white text-base  rounded-full w-12 h-12  flex justify-center items-center focus:outline-none "
                  style={{
                    backgroundColor: "#a62626",
                    outlineStyle: "none",
                  }}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="btnActiveRed h-8 w-8 rounded-full focus:outline-none "
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path d="M2 3a1 1 0 011-1h2.153a1 1 0 01.986.836l.74 4.435a1 1 0 01-.54 1.06l-1.548.773a11.037 11.037 0 006.105 6.105l.774-1.548a1 1 0 011.059-.54l4.435.74a1 1 0 01.836.986V17a1 1 0 01-1 1h-2C7.82 18 2 12.18 2 5V3z" />
                  </svg>
                </button>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
}

export default MobileFooter;
