import React from "react";

function SafeBuy() {
  return (
    <div>
      <div
        className="flex justify-end font-light border-b-2 pb-4 text-sm "
        style={{ color: "#707070" }}
      >
        {" "}
        راهنمای خرید امن
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="h-6 w-6 ml-1"
          viewBox="0 0 20 20"
          fill="currentColor"
          style={{ color: "#a62626", opacity: "80%" }}
        >
          <path
            fillRule="evenodd"
            d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-8-3a1 1 0 00-.867.5 1 1 0 11-1.731-1A3 3 0 0113 8a3.001 3.001 0 01-2 2.83V11a1 1 0 11-2 0v-1a1 1 0 011-1 1 1 0 100-2zm0 8a1 1 0 100-2 1 1 0 000 2z"
            clipRule="evenodd"
          />
        </svg>
      </div>
      <div
        className="flex justify-end font-light border-b-2 py-4 text-sm"
        style={{ color: "#707070" }}
      >
        گزارش مشکل آگهی
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="h-6 w-6 ml-1"
          viewBox="0 0 20 20"
          fill="currentColor"
          style={{ color: "#a62626", opacity: "80%" }}
        >
          <path
            fillRule="evenodd"
            d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
            clipRule="evenodd"
          />
        </svg>
      </div>
    </div>
  );
}

export default SafeBuy;
