import React from "react";
import { SocialIcon } from "react-social-icons";

function Footer() {
  return (
    <div>
      <footer className="pt-28 mb-2  ">
        <ul className="flex flex-col-reverse justify-center lg:flex-row-reverse lg:justify-between lg:grid lg:grid-cols-9">
          <ul className="flex lg:col-start-3 pl-4 justify-center mr-6 mb-12 md:mb-2">
            <li>
              <a href="#">
                <img
                  className="h-6 w-6 mt-1.5"
                  alt="Aparat"
                  src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAAIpklEQVRoge1ZfYwUZxn/PTNzw22B3pUCtVaqUikNFzjZeedue1ypV2kRSAmKoVWgwVRIm0YFUwXTWqiSKB9VgylNa2qlFWrA2ognH1K72F7XvZuZhUWvKa5QCyQWUHq5oge7s/P4x+4ss3Ozs8vl2vpHf3/t8/E+7++Z93k/F/gQHyzochvE43Fl9OjRGjNPLKqONTY2pidNmnRxmLnVhJoT2Llzpzxx4sQHAKwG8FGf+SIAk4heA9DV399/oKOj48Iw8qyImhIwTfMKIvo1M88JMGcBvARgPzO/SkRHhRD/HVaWIVBq9HsqgPzbAH5SV1f3THNz85lh5lUzqo5AT0/PrZIkHfSosgA2RiKRDU1NTeffO2q1oeoISJK03CO+zsxLdF0/5PdLp9Pjs9nsZyVJmsXMTQA+BmA8CvPjPDOfIiKDiHqYuVMI8S9/jFQqNcVxnHuI6KjjOAld149W4xc6AsxMlmWdBjAOwIELFy4sbG9vf9fX6c2O46wC8HnUXpI2M++VZXlzNBp9xWvo6en5iCzLLzBzG4CzRLQPwM80TXv1shPo7u6+UZblowBejEQidzc1NWVd2+HDh6/L5/NbmXl+jaQrYQ+ArwshjrmKRCIRUVV1B4AFHr9fNDQ03OdfrqWwyIqizADwWjabXewlbxjG3bZt9w4DeQCYC+CQZVlLXEVbW9tAJBK5i4he8vgt6+vr28XMZZxDE2DmG23bXtjW1jbgIf9tItoBoGEYyLsYzczPGYaxzlU0NTVlc7ncQgBvuDoiujOVSt3vbRiagCRJz8VisdOubJrmI0S0AcGldxzADgDPAxjSskpEay3LWuvKsVisH8BiFFY+AAAzr/GOQs07sWEYC4loV0AbB8Cq48ePP75o0aI8AMTj8fpRo0ZtJaKvDCURZv6yruvPu7JlWeuZ+aESaSKhaZoFVBkBF4cOHfoEEW0LIA8AG4QQW1zyANDR0XHh/PnzKwAcDPCvCiJ6qru7+5OunMvlNqJ8VG9wf9SUQD6f3wRgZIDJAfCjoDYdHR02M2+qEvqdYgw/Rsmy/FNXiMVi/cy8xWMvfciqCaRSqZkAvljBfCZoQyr1QhS0EQ0A+AaAkUKIMaqqjmXmRzE4kXmGYcxwBWZ+GkAOABzHebPmBBzHeSDEPCaRSEQqGZl5QoB6gRBii3vgmzZt2ju6rq9j5u/4HYnoW+7vlpaWtwG8DOBiY2NjuqYEjhw5chWAsLVeVVX1q5WMROS37RdC/CHIVwixCcDffOq56XR6vCsw834ApnczC00gl8stAFAf5gNgs2EYt3sVzEzFNX2xT/9ypSBExAASPnVdLpcrnYIlSXqleOcoIfTswswzq5AHAJWI9lmW1ek4Tg8RqaZpziUiEUByRJVYQfbPANgGAIqivGHbdpfXWO3w1VzF7kJi5vlENL9ItJLfPGZeX/zaZSjOpVkBbaaWyDQ3/yeTyZSVYLVJ/PEq9jDssW17gm3bE4hob1HXalnWw37HeDyuqKq6FYVTrx/XewX/YS50JzZNM4faj8hlsG17QiwWOwUAPT09EyRJOlE0MRH9Mp/PP01EJyVJmgLgQWa+tUKorBCiYulVIxeWYJyIfkxEx2zbHifL8r3MvMRtI0lSpbbEzEslSVoKAMyDqsmPoI2uhGoJ9AG4ehADop9Ho9HlROQN/ifTNDMAvgcAsiw/mUwmVyiKIjHzk9VYhqA/zFg2BzKZjH+oTmAwzg0MDKz0kQcAaJq2HsBfAICZ5yiKchLAW0T0ucvjXIZ/hBnLEujv77/DNM0rXJmI/hrQZp//WunxZyLaXaGvNID1zPxw0Se0NDw4Emb0l1A7M58C4F7a4wCW+kiGPlgx86CXCmZeLYTY5F0+DcO4nYg6Aahh8fwblx9lI8DMM4joFlfO5XJ7ANh+H2auOLmLl3Evdum6vtG/9uu6foCZ14SRA5Czbft3NSVQrH8BYLari8Vip5l5r6/NZNM0VwQFsyxrNhHN8+qYeWulzh3HeRa+D+RDZ2tr679D7JcS6Ovra0ZhK5+VTCavcfVEtNHfiIgeNwxjXSKRGAMApmmONU3zm8y8G75RdRznrUqdF8mdq2Rn5sfCyJclgEu3HFVRlNJVUAjRBeD3vnYyEa1VVfWsaZrnAPwTwGMIqGdFUSZX6rz4AcZWMP9W1/XQ+vcn4K3RVV1dXaNdIZ/Pfw1A0DOiBOAqhOwnzLyq0pxRVXUlgo8z/bIsrwwj7iUAAJBl+e8e/fhIJLLaFVpbW99k5sC6rwF3WJa1NR6Pl47lzEymaa4G8FCAPzPzfdOnTw9d/12UvgwzS5ZlnQBwXVGVBXCzECLl+liWtZaZ12FoOAPgjyi8lbYD+FQFv0eEEN93hVQqNSUajb5eKeil9xUih4h+4LGpALYnk8krXYWmaY8W769DwXgAXwKwLIT8d73kTdO81nGce8KCltVfNBp9AuUT9qa6uroXent7S5NT1/V1RLQEQOBuPES8S0R3CSHWu4ri/eA3FR4GSihLgIichoaGhSjegACAmWcNDAzs9F7eNU3bzsyfBtA5DOQ7JUmaqmnaTlfR29urjhgx4lcAYrZth65EFXdU0zTbASwHMAfAOGb+MzN/ofg64Pd7EIVH2roaSecAdDqOs7mlpaXsHpxMJq9UFOVFALcBOKtp2jVBN7iqCXhhGMZkSZLamHmyJEnPBk2q7u7uqxVFuRPALQCmMvP1KDwAKyhsVieZ+QiAg7Is741Go2f9MSzL0ph5OwB379gmhFgWxu2y/2Z9L1D86mtQGEl3FNlxnHb/CPkxpOvicME0zWsB3IvCS13ZjkxEz1QjD7zPCaTT6ZG2bd/kOM5MIpqNQp0PmjdEtLu+vv7+wREG431LIB6P19u2fRuAdiKagcLJ10/+JIAfRqPRJ8Imrhcf2BzIZDIjiifgGyRJyjuOkxFCpIOuqh/i/xn/AwaAZSZq0Y26AAAAAElFTkSuQmCC"
                />
              </a>
            </li>
            <li>
              {" "}
              <SocialIcon
                style={{
                  height: "20px",
                  width: "20px",
                  marginLeft: "9px",
                  marginTop: "9px",
                }}
                url="https://www.linkedin.com"
                bgColor="#cccccc"
              />
            </li>
            <li>
              {" "}
              <SocialIcon
                style={{
                  height: "20px",
                  width: "20px",
                  marginLeft: "9px",
                  marginTop: "9px",
                }}
                url="https://www.instagram.com"
                bgColor="#cccccc"
              />
            </li>
            <li>
              {" "}
              <SocialIcon
                style={{
                  height: "20px",
                  width: "20px",
                  marginLeft: "9px",
                  marginTop: "9px",
                }}
                url="https://www.twitter.com"
                bgColor="#cccccc"
              />
            </li>
          </ul>
          <ul className="flex flex-col-reverse lg:flex-row lg:col-start-5  lg:col-end-8 text-right text-gray-400 text-sm font-thin h-5 pt-0.5  lg:justify-start ">
            <ul className="flex lg:col-start-6 lg:col-end-8 text-right text-gray-400 text-sm font-thin h-5 pt-0.5 justify-center  lg:justify-start mt-0.5 mb-2 lg:mb-0">
              <li className="border-r-2 px-2 .footer">
                <a href="#">بلاگ دیوار</a>
              </li>

              <li className="border-r-2 px-2 .footer">
                <a href="#">پشتیبانی و قوانین</a>
              </li>
              <li className=" px-2 .footer">
                <a href="#">درباره دیوار</a>
              </li>
            </ul>
            <ul className="flex lg:col-start-6 lg:col-end-9 text-right  text-3xl font-thin h-5  justify-center ml-6 mb-6">
              <li className=" pr-4 ">
                <a style={{ color: "#a62626", opacity: "70%" }} href="#">
                  دیوار
                </a>
              </li>
            </ul>
          </ul>
        </ul>
      </footer>
    </div>
  );
}

export default Footer;
