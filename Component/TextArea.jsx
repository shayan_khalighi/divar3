import React from "react";

function TextArea() {
  return (
    <div>
      <textarea
        rows="5"
        type="text"
        className="rounded-md border text-right w-full h-500"
        placeholder="...یادداشت شما"
      />
      <span
        className="font-extralight  text-xs mr-2 text-xs"
        style={{ color: "#707070" }}
      >
        .یادداشت تنها برای شما قابل دیدن است و پس از حذف آگهی، پاک خواهد شد
      </span>
    </div>
  );
}

export default TextArea;
