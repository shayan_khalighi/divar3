export default function DesktopNav() {
  return (
    <nav className="shadow-md fixed z-50 bg-white w-full  border border-b-gray mb-8 md:visible  invisible">
      <ul className=" flex  justify-between ">
        <ul className="flex mt-2 ml-4">
          <li className="mr-6 p-2">
            <a
              className="text-white px-4 py-2 rounded-md text-sm"
              href="#"
              style={{ backgroundColor: "#a62626" }}
            >
              ثبت آگهی
            </a>
          </li>
          <li className="mr-6 pt-2">
            <a className="text-xs" href="#" style={{ color: "#707070" }}>
              خروج
            </a>
          </li>
          <li className="mr-6 p-2">
            <a style={{ color: "#707070" }} className="text-xs" href="#">
              پشتیبانی
            </a>
          </li>
          <li className="mr-6 p-2">
            <a style={{ color: "#707070" }} className="text-xs" href="#">
              بلاگ
            </a>
          </li>
          <li className="mr-6 p-2">
            <a style={{ color: "#707070" }} className="text-xs" href="#">
              درباره دیوار
            </a>
          </li>
          <li className="mr-6 p-2">
            <a style={{ color: "#707070" }} className="text-xs" href="#">
              چت
            </a>
          </li>
          <li className="mr-6 p-2">
            <a style={{ color: "#707070" }} className="text-xs" href="#">
              دیوار من
            </a>
          </li>
        </ul>
        <ul className="flex  justify-between ">
          <li className="p-2 pr-2">
            <a
              style={{ color: "#707070" }}
              className="flex mt-3 text-sm"
              href="/home"
            >
              انتخاب شهر{" "}
              <svg
                className="h-4 w-4 text-black ml-1 "
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z"
                />
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M15 11a3 3 0 11-6 0 3 3 0 016 0z"
                />
              </svg>
            </a>
          </li>
          <li className="pr-4">
            <a href="/home">
              <img className="h-14" src="/images/logo.png" alt="دیوار" />
            </a>
          </li>
        </ul>
      </ul>
    </nav>
  );
}
