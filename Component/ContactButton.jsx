import React from "react";

function ContactButton() {
  return (
    <div className="flex justify-between md:flex-col-reverse  flex-col-reverse md:flex-row mt-8 md:visible  invisible absolute md:static">
      <div className="flex justify-between  lg:justify-between ml-10  mt-8 md:mt-3">
        <svg
          className="h-5 w-5  ml-4 "
          width="24"
          height="24"
          viewBox="0 0 24 24"
          strokeWidth="2"
          stroke="currentColor"
          fill="none"
          strokeLinecap="round"
          strokeLinejoin="round"
          style={{ color: "#707070" }}
        >
          {" "}
          <path stroke="none" d="M0 0h24v24H0z" />{" "}
          <circle cx="6" cy="12" r="3" /> <circle cx="18" cy="6" r="3" />{" "}
          <circle cx="18" cy="18" r="3" />{" "}
          <line x1="8.7" y1="10.7" x2="15.3" y2="7.3" />{" "}
          <line x1="8.7" y1="13.3" x2="15.3" y2="16.7" />
        </svg>
        <svg
          className="h-5 w-5  ml-2"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
          style={{ color: "#707070" }}
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="2"
            d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z"
          />
        </svg>
      </div>
      <div className="flex justify-between ">
        <button
          style={{
            color: "#707070",
            borderColor: "#858585",
            outlineStyle: "none",
          }}
          className="text-base border  h-8 w-40 rounded  text-gray-500 active:bg-gray-200"
        >
          چت
        </button>

        <button
          className="btnActiveRed  text-base  text-white h-8 w-40 rounded  "
          style={{ backgroundColor: "#a62626", outlineStyle: "none" }}
        >
          اطلاعات تماس
        </button>
      </div>
    </div>
  );
}

export default ContactButton;
