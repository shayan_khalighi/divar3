import React from "react";

function Description() {
  return (
    <div>
      <div
        className="mt-8 space-y-8 leading-relaxed "
        style={{ color: "#212121" }}
      >
        <span className="text-xl font-black "> توضیحات</span>
        <br />
        <br />
        <span className="font-thin text-base ">پاترول چهار در چهار سیلندر</span>
        <br />
        <span className="font-thin text-base ">فنی عالی</span>
        <br />
        <span className="font-thin text-base ">
          تودوزی پاجیرویی و کنسول وسط
        </span>
        <br />
        <span className="font-thin text-base  ">لاستیک جلو صد درصد</span>
        <br />
        <span className="font-thin text-base ">اطاق تعویض قید در سند</span>
        <br />
      </div>
    </div>
  );
}

export default Description;
